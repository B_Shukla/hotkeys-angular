import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommandService, Command } from '../services/command.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  subscription: Subscription;
  
  constructor(private commandService: CommandService,) {
    this.subscription = this.commandService.commands.subscribe(c => {
      this.handleCommand(c)
    });
   }

  ngOnInit() {
  }

  handleCommand(command: Command) {
    switch (command.name) {
      case 'mainComponent.fisrtBtn':
        document.getElementById("firstBtn").click();
        break;

      case 'mainComponent.secondBtn':
        document.getElementById("secondBtn").click();
        break;
    }
  }

}
