import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FirstComponent } from "./first/first.component";
import { SecondComponent } from "./second/second.component";
import { MainComponent } from './main/main.component';

const routes: Routes = [
  {
    path: '', redirectTo: 'main', pathMatch:'full'
  },
  {
    path: 'main', component: MainComponent,
    data: { heading: 'main Component', breadcrumb: 'Main Component' }
  },
  {
    path: 'first', component: FirstComponent,
    data: { heading: 'First Component', breadcrumb: 'Main Component/First Component' }
  },
  {
    path: 'second', component: SecondComponent,
    data: { heading: 'Second Component', breadcrumb: 'Main Component/Second Component' }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
