import { Component, OnInit } from '@angular/core';
import { Command, CommandService } from '../services/command.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.scss']
})
export class FirstComponent implements OnInit {
  subscription: Subscription;
  
  constructor(private commandService: CommandService,) {
    this.subscription = this.commandService.commands.subscribe(c => {
      this.handleCommand(c)
    });
   }

  ngOnInit() {
  }

  handleCommand(command: Command) {
    switch (command.name) {
      case 'firstComponent.mainBtn':
        document.getElementById("mainBtn").click();
        break;

      case 'firstComponent.secondBtn':
        document.getElementById("secondBtn").click();
        break;
    }
  }

}
