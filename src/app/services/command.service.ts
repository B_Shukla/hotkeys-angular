import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HotkeysService, Hotkey } from 'angular2-hotkeys';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
class HotkeyConfig {
  [key: string]: string[];
}

class ConfigModel {
  hotkeys: HotkeyConfig;
}

export class Command {
  name: string;
  combo: string;
  ev: KeyboardEvent;
}

@Injectable({
  providedIn: 'root'
})
export class CommandService {
  private subject: Subject<Command>;
  commands: Observable<Command>;

  constructor(private hotkeysService: HotkeysService,
    private http: HttpClient,) {

    this.subject = new Subject<Command>();
    this.commands = this.subject.asObservable();
    this.http.get('assets/config.json').toPromise()
      .then(r => r as ConfigModel)
      .then(c => {
        for (const key in c.hotkeys) {
          const commands = c.hotkeys[key];
          const allowIn = ['INPUT', 'SELECT', 'TEXTAREA'];
          hotkeysService.add(new Hotkey(key, (ev, combo) => this.hotkey(ev, combo, commands), allowIn));
        }
      });
  }

  hotkey(ev: KeyboardEvent, combo: string, commands: string[]): boolean {
    commands.forEach(c => {
      const command = {
        name: c,
        ev: ev,
        combo: combo
      } as Command;
      this.subject.next(command);
    });
    return true;
  }
}
