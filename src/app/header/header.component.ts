import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from "@angular/router";
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
 breadcrumb:string = "";
 title:string = "";
  constructor(private activatedRoute:ActivatedRoute,
    private router:Router) { }

  ngOnInit() {
    this.router.events.subscribe(val=>{
      if(val instanceof NavigationEnd){

        var currUrl = this.router.url.replace('/','');
        var currRoute = this.router.config.find((route:any)=>{
          if(route.path == currUrl)
          return route
        });
        if(currRoute.data){
  
          this.breadcrumb = currRoute.data.breadcrumb
        }
      }
    });
  }

}
