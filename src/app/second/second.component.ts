import { Component, OnInit } from '@angular/core';
import { Command, CommandService } from '../services/command.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.scss']
})
export class SecondComponent implements OnInit {
  subscription: Subscription;
  
  constructor(private commandService: CommandService,) {
    this.subscription = this.commandService.commands.subscribe(c => {
      this.handleCommand(c)
    });
   }

  ngOnInit() {
  }

  handleCommand(command: Command) {
    switch (command.name) {
      case 'secondComponent.mainBtn':
        document.getElementById("mainBtn").click();
        break;

      case 'secondComponent.fisrtBtn':
        document.getElementById("fisrtBtn").click();
        break;
    }
  }

}
